package com.example.loadimg;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.RelativeLayout;

public class LoadImg extends Activity {

	RelativeLayout rl1;
	String url;
	Bitmap b;
	public String fp;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_load_img);
		
		url = "http://agusprayogo.com/mu2.jpg";
		rl1 = (RelativeLayout) findViewById(R.id.rl1);
				
		getDatas g = new getDatas();
		g.execute(url);
	}
	
	private class getDatas extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... urls) {
			String filepath = "";
			String Surl = urls[0];
			String filename = Surl.substring(Surl.lastIndexOf("/") + 1);
			
			try{   
				  File Root = new File(getFilesDir() + "/mclub_images");
				  Root.mkdirs();   
				  Log.i("Local filename:",""+filename);
				  File file = new File(Root,filename);
				  
				  if (!file.exists()){
					  URL url = new URL(Surl);
					  HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
					  urlConnection.setRequestMethod("GET");
					  urlConnection.setDoOutput(false);
					  urlConnection.connect();
					  
						if (file.createNewFile()) {
							file.createNewFile();
						}
						FileOutputStream fileOutput = new FileOutputStream(file);
						InputStream inputStream = urlConnection.getInputStream();
						int totalSize = urlConnection.getContentLength();
						int downloadedSize = 0;
						byte[] buffer = new byte[1024];
						int bufferLength = 0;
						while ((bufferLength = inputStream.read(buffer)) > 0) {
							fileOutput.write(buffer, 0, bufferLength);
							downloadedSize += bufferLength;
							Log.i("Progress:", "downloadedSize:" + downloadedSize
									+ "totalSize:" + totalSize);
						}
						fileOutput.close();

						if (downloadedSize == totalSize)
							filepath = file.getPath();
				  }else{
					  filepath = file.getPath();
				  }
			} 
			catch (MalformedURLException e) 
			{
			  e.printStackTrace();
			} 
			catch (IOException e)
			{
			  filepath=null;
			  e.printStackTrace();
			}
			Log.i("filepath:"," "+filepath) ;
			
			fp = filepath;			
			return filepath;
		}

		@Override
		protected void onPostExecute(String result) {
			Drawable d = getGambar(fp);
			rl1.setBackgroundDrawable(d);
		}
	}
	
	public Drawable getGambar(String path){
		Bitmap bmp = BitmapFactory.decodeFile(path);
		Drawable d = new BitmapDrawable(getResources(),bmp);
		return d;
	}
}