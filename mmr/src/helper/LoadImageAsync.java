package helper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class LoadImageAsync extends AsyncTask<String, Void, String> {

	Context c;
	RelativeLayout r;
	ImageView iv;
	boolean isRl = true;
	String fp;
	File file;

	public LoadImageAsync(Context _c, RelativeLayout _r) {
		c = _c;
		r = _r;
	}
	
	public LoadImageAsync(Context _c,ImageView _r) {
		c = _c;
		iv = _r;
		isRl = false;
	}

	@Override
	protected String doInBackground(String... urls) {
		String filepath = "";
		String Surl = urls[0];
		String filename = Surl.substring(Surl.lastIndexOf("/") + 1);
		Log.i("filename", filename);

		try {
			file = new File(c.getFilesDir().getPath().toString(), filename);
			if (!file.exists()) {
				URL url = new URL(Surl);
				HttpURLConnection urlConnection = (HttpURLConnection) url
						.openConnection();
				urlConnection.setRequestMethod("GET");
				urlConnection.setDoOutput(false);
				urlConnection.connect();

				if (file.createNewFile()) {
					file.createNewFile();
				}
				FileOutputStream fileOutput = new FileOutputStream(file);
				InputStream inputStream = urlConnection.getInputStream();
				int totalSize = urlConnection.getContentLength();
				int downloadedSize = 0;
				byte[] buffer = new byte[1024];
				int bufferLength = 0;
				while ((bufferLength = inputStream.read(buffer)) > 0) {
					fileOutput.write(buffer, 0, bufferLength);
					downloadedSize += bufferLength;
					Log.i("Progress:", "downloadedSize:" + downloadedSize
							+ "totalSize:" + totalSize);
				}
				fileOutput.close();

				if (downloadedSize == totalSize)
					filepath = file.getPath();
			} else {
				filepath = file.getPath();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			filepath = null;
			e.printStackTrace();
		}
		Log.i("filepath:", " " + filepath);

		fp = filepath;
		return filepath;
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onPostExecute(String result) {
		Drawable d = getGambar(fp);
		if(isRl){
			r.setBackgroundDrawable(d);
		}else{
			iv.setTag(d);
			iv.setImageDrawable(d);
		}
	}

	public Drawable getGambar(String path) {
		Bitmap bmp = BitmapFactory.decodeFile(path);
		Drawable d = new BitmapDrawable(c.getResources(), bmp);
		return d;
	}

	public File getFile() {
		return this.file;
	}
}