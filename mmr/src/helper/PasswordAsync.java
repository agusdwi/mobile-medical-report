package helper;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public abstract class PasswordAsync extends AsyncTask<String, Void, String> implements ClientInterface {

	private Context mainContext;
	private Activity CallerActivity;
	private ProgressDialog pd;
	
	public PasswordAsync(Context c, Activity a) {
		mainContext = c;
		CallerActivity = a;
	}
	
	protected void onPreExecute() {
		pd = new ProgressDialog(mainContext);
		pd.setMessage("Contacting server...");
		pd.setCancelable(false);
		pd.show();
	}
	
	public abstract void onResponseReceived(String sReturn);

	protected String doInBackground(String... params) {
		String sReturn = "";
		String token = Token.getToken();
		String url = CommonUtilities.SERVER_URL + "register";
		
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("no_rekmed", params[0]));
		pairs.add(new BasicNameValuePair("password", params[1]));
		pairs.add(new BasicNameValuePair("_token", token));

		try {
			sReturn = HTTPReq.postRequest(url, pairs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sReturn;
	}

	protected void onPostExecute(String sReturn) {
		if (pd.isShowing()) {
			pd.dismiss();
		}
		onResponseReceived(sReturn);
	}
	
}