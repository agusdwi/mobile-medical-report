package helper;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public abstract class LoginAsync extends AsyncTask<String, Void, String>{

	private Context mainContext;
	private ProgressDialog pd;
	
	public LoginAsync(Context c) {
		mainContext = c;
	}
	
	protected void onPreExecute() {
		pd = new ProgressDialog(mainContext);
		pd.setMessage("Sending data...");
		pd.show();
		pd.setCancelable(false);
	}
	
	public abstract void onResponseReceived(String sReturn);

	protected String doInBackground(String... params) {
		String sReturn = "";
		String token = Token.getToken();
		String url = CommonUtilities.SERVER_URL + "login";
		
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("no_rekmed", params[0]));
		pairs.add(new BasicNameValuePair("password", params[1]));
		pairs.add(new BasicNameValuePair("regId", params[2]));
		pairs.add(new BasicNameValuePair("_token", token));
		
		try {
			sReturn = HTTPReq.postRequest(url, pairs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sReturn;
	}

	protected void onCancelled() {}

	protected void onPostExecute(String sReturn) {
		pd.dismiss();
		onResponseReceived(sReturn);
	}
}