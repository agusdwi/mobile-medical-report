package helper;

import android.content.Context;
import android.content.Intent;

public final class CommonUtilities {

	public static final String SERVER_URL 	= "http://192.168.1.11/simrs_khayangan/api/";
    public static final String SENDER_ID 	= "162707133455";
    public static final String API_KEY 		= "AIzaSyBfeSRh8mglzQL4yj0zQmFEjwyOonVZnrU";
    
    public static final String PREFS_NAME 			= "AppPrefs";
	public static final String PREFS_LOGIN_STATE 	= "login_state";
	public static final String PREFS_TOKEN 			= "token";
	public static final String PREFS_NAMA_PASIEN 	= "nama";
	public static final String PREFS_REGID 			= "reg_id";
	public static final String PREFS_REKMED 		= "rekmed";
	public static final String PREFS_INIT_DATA 		= "init_data";
	public static final String PREFS_QRCODE 		= "qrcode";
	public static final String PREFS_PROFPIC 		= "profpic";
	public static final String PREFS_ALAMAT 		= "alamat";
	
	public static final String STATE_NOT_LOGIN 			= "0";
	public static final String STATE_PENDING_APPROVAL 	= "1";
	public static final String STATE_APPROVAL_INIT 		= "2";
	public static final String STATE_ALREADY_LOGIN 		= "3";
	
	/**
     * Tag used in core application.
     */		
	
	public static final String JADWAL_LAST_TIME = "jadwal_last_time";
	public static final String RIWAYAT_LAST_TIME = "riwayat_last_time";
	
	public static final String PRIVATE_KEY 		= "agusdwi";
 
    /**
     * Tag used on log messages.
     */
    public static final String TAG = "AndroidHive GCM";
 
    public static final String DISPLAY_MESSAGE_ACTION =
            "com.app.mmr.DISPLAY_MESSAGE";
 
    static final String EXTRA_MESSAGE = "message";
 
    static void displayMessage(Context context, String message) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }
    
    static public String indoDay(String day){
		Days d = Days.valueOf(day.toUpperCase());
		String rt = "";
		switch (d) {
			case SUNDAY:rt = "Minggu";break;
			case MONDAY:rt = "Senin";break;
			case TUESDAY:rt = "Selasa";break;
			case WEDNESDAY:rt = "Rabu";break;
			case THURSDAY:rt = "Kamis";break;
			case FRIDAY:rt = "Jumat";break;
			case SATURDAY:rt = "Sabtu";break;
		}
		return rt;
	}
	
	static public enum Days {
	    MONDAY,
	    TUESDAY,
	    WEDNESDAY,
	    THURSDAY,
	    FRIDAY,
	    SATURDAY,
	    SUNDAY
	  }
}