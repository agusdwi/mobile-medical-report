package helper;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public abstract class InitDataLoginAsync extends
		AsyncTask<String, Void, String> implements ClientInterface {

	private Context mainContext;
	private ProgressDialog pd;

	public InitDataLoginAsync(Context c, Activity a) {
		mainContext = c;
	}

	protected void onPreExecute() {
		pd = new ProgressDialog(mainContext);
		pd.setMessage("Contacting Server data...");
		pd.show();
		pd.setCancelable(false);
	}

	public abstract void onResponseReceived(String sReturn);

	protected String doInBackground(String... params) {

		String sReturn = "";	
		String token = Token.getToken();
		String url = CommonUtilities.SERVER_URL + "getInitData/";
		
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("no_rekmed", params[0]));
		pairs.add(new BasicNameValuePair("_token", token));

		try {
			sReturn = HTTPReq.postRequest(url, pairs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sReturn;

	}

	protected void onPostExecute(String sReturn) {
		pd.dismiss();
		onResponseReceived(sReturn);
	}

}
