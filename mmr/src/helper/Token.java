package helper;

import static helper.CommonUtilities.*;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class Token {
	public static String getToken() {

		String sReturn = null;
		String sResult = "";

		String url = CommonUtilities.SERVER_URL + "token/";
		try {
			sReturn = HTTPReq.getRequest(url);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			if(sReturn == null){
				Log.d("error", "connection error");
			}else{
				JSONObject jObject = new JSONObject(sReturn);
				String apiResult = jObject.getString("success").toString();
				if (apiResult.equals("1")) {
					sResult = jObject.getString("token").toString();
				}
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return sResult;
	}
	
	public static String getPrivateToken(Context c){
		String sResult;
		SharedPreferences settings = c.getSharedPreferences(PREFS_NAME, 0);
		sResult = settings.getString(PREFS_TOKEN, "");
		return sResult;
	}
}
