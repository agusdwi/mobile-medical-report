package helper;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;

public abstract class CheckActiveAsync extends AsyncTask<String, Void, String> implements ClientInterface{

	private Context mainContext;
	private JSONObject jObject;
	private Activity CallerActivity;
	
	public CheckActiveAsync(Context c, Activity a) {
		mainContext = c;
		CallerActivity = a;
	}
	
	public abstract void onResponseReceived(String sReturn);

	protected String doInBackground(String... params) {
		
		String url = CommonUtilities.SERVER_URL + "pendaftaran/cekaktif/";
		String sReturn = "";
		HttpClient client = new DefaultHttpClient();
		HttpPost request = new HttpPost(url);
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();

		pairs.add(new BasicNameValuePair("rekmed", params[0]));

		try {
			request.setEntity(new UrlEncodedFormEntity(pairs));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Making HTTP Request
		try {

			HttpResponse response = client.execute(request);
			InputStream in = response.getEntity().getContent();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(in));
			StringBuilder str = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				str.append(line + "\n");
			}
			in.close();
			sReturn = str.toString();

		} catch (Exception e) {
			// writing exception to log
			e.printStackTrace();
		}

		return sReturn;

	}

	protected void onCancelled() {

	}

	protected void onPostExecute(String sReturn) {
		onResponseReceived(sReturn);
	}
}