package helper;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.util.Log;

public  class HTTPReq {

	static int timeoutConnection = 3000;
	static int timeoutSocket = 5000;
	
	public static String postRequest(String url,List<NameValuePair> pairs) throws Exception{	
		HttpParams httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		
		String sReturn = null;
		HttpClient client = new DefaultHttpClient(httpParameters);
		HttpPost request = new HttpPost(url);
		
		try {
			request.setEntity(new UrlEncodedFormEntity(pairs));
			request.addHeader("private-key", CommonUtilities.PRIVATE_KEY);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		try {
			HttpResponse response = client.execute(request);
			InputStream in = response.getEntity().getContent();
			BufferedReader reader = new BufferedReader(
				new InputStreamReader(in));
			StringBuilder str = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				str.append(line + "\n");
			}
			in.close();
			sReturn = str.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Log.d("httpreq",sReturn);
		
		return sReturn;
	}
	
	public static String getRequest(String url){
		HttpParams httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		
		String sReturn = null;
		
		HttpClient client = new DefaultHttpClient(httpParameters);
		HttpGet request = new HttpGet(url);

		try {
			request.addHeader("private-key", CommonUtilities.PRIVATE_KEY);
			HttpResponse response = client.execute(request);
			InputStream in = response.getEntity().getContent();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(in));
			StringBuilder str = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				str.append(line + "\n");
			}
			in.close();
			sReturn = str.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return sReturn;
	}
}