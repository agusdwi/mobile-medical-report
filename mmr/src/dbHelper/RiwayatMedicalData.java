package dbHelper;

public class RiwayatMedicalData {
	
	String tgl,dokter,poli,id;
	
	public RiwayatMedicalData() {
		// TODO Auto-generated constructor stub
	}
	
	public RiwayatMedicalData(String _tgl,String _dokter,String _poli,String _id) {
		tgl	= _tgl;
		dokter	= _dokter;
		poli	= _poli;
		id	= _id;
	}
	
	public void setDokter(String dokter) {
		this.dokter = dokter;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public void setPoli(String poli) {
		this.poli = poli;
	}
	
	public void setTgl(String tgl) {
		this.tgl = tgl;
	}
	
	public String getDokter() {
		return dokter;
	}
	
	public String getId() {
		return id;
	}
	
	public String getPoli() {
		return poli;
	}
	
	public String getTgl() {
		return tgl;
	}

}
