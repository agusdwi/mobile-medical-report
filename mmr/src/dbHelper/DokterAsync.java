package dbHelper;

import helper.CommonUtilities;
import helper.HTTPReq;
import helper.Token;

import java.util.ArrayList;

import com.app.mmr.R;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

public abstract class DokterAsync extends AsyncTask<String, Void, String>{

	private Context mainContext;
	private ProgressDialog pd;
	ArrayList<DokterData> dd;
	
	public DokterAsync(Context c) {
		mainContext = c;
	}
	
	protected void onPreExecute() {
		pd = new ProgressDialog(mainContext);
		pd.setMessage("Sending data...");
		pd.show();
		pd.setCancelable(false);
	}
	
	public abstract void onResponseReceived(ArrayList<DokterData> sReturn);

	protected String doInBackground(String... params) {
		DataBaseHelper db = DataBaseHelper.initDb(mainContext);
		dd = db.getDataDokter();
		
		Log.d("size", Integer.toString(dd.size()));
		
		if(!(dd.size() > 0)){
			String sReturn = "";
			String token = Token.getPrivateToken(mainContext);
			String url = CommonUtilities.SERVER_URL + "dokter/"+token;
			
			try {
				sReturn = HTTPReq.getRequest(url);
				Log.d("return",sReturn);
				dd = db.setDataDokter(sReturn);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}	
		
		return "";
	}

	protected void onCancelled() {

	}

	protected void onPostExecute(String sReturn) {
		pd.dismiss();
		onResponseReceived(dd);
	}
}