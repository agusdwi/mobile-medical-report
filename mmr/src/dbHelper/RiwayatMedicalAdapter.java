package dbHelper;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.mmr.R;

public class RiwayatMedicalAdapter extends ArrayAdapter<RiwayatMedicalData>{

	int resource;
	Context context;
	List<RiwayatMedicalData> items;
	
	public RiwayatMedicalAdapter(Context context, int resource, List<RiwayatMedicalData> items) {
		super(context, resource, items);
		this.resource = resource;
		this.context = context;
		this.items = items;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LinearLayout ktView;

		RiwayatMedicalData item = getItem(position);

		String dokter 	= item.getDokter(), 
				poli 	= item.getPoli(), 
				date 	= item.getTgl();
		
		if (convertView == null) {
			ktView = new LinearLayout(getContext());
			String inflater = Context.LAYOUT_INFLATER_SERVICE;
			LayoutInflater li;
			li = (LayoutInflater) getContext().getSystemService(inflater);
			li.inflate(resource, ktView, true);
		} else {
			ktView = (LinearLayout) convertView;
		}
				
		TextView rowName 	= (TextView) ktView.findViewById(R.id.textView3);
		TextView rowJam 	= (TextView) ktView.findViewById(R.id.textView4);
		TextView rowPoli 	= (TextView) ktView.findViewById(R.id.poli);
			
		rowName.setText(dokter);
		rowJam.setText(date);
		rowPoli.setText(poli);
		
		return ktView;
	}	
}