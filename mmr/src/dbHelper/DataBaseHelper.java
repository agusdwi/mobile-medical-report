package dbHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataBaseHelper extends SQLiteOpenHelper {

	private static String DB_PATH = "";
	private static String DB_NAME = "mmrdb.sqlite";

	private SQLiteDatabase mDataBase;
	private final Context mContext;
	String[] hari = { "senin", "selasa", "rabu", "kamis", "jumat", "sabtu",
			"minggu" };

	public static DataBaseHelper initDb(Context c) {
		DataBaseHelper db = new DataBaseHelper(c);

		try {
			db.createDataBase();
		} catch (IOException ioe) {
			throw new Error("Unable to create database");
		}

		try {
			db.openDataBase();
		} catch (SQLException sqle) {
			throw sqle;
		}
		return db;
	}

	public DataBaseHelper(Context context) {
		super(context, DB_NAME, null, 1);// 1? its Database Version
		if (android.os.Build.VERSION.SDK_INT >= 4.2) {
			DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
		} else {
			DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
		}
		this.mContext = context;
	}

	public void createDataBase() throws IOException {
		// If database not exists copy it from the assets
		boolean mDataBaseExist = checkDataBase();
		if (!mDataBaseExist) {
			this.getReadableDatabase();
			this.close();
			try {
				copyDataBase();
			} catch (IOException mIOException) {
				throw new Error("ErrorCopyingDataBase");
			}
		}
	}

	private boolean checkDataBase() {
		File dbFile = new File(DB_PATH + DB_NAME);
		return dbFile.exists();
	}

	private void copyDataBase() throws IOException {
		InputStream mInput = mContext.getAssets().open(DB_NAME);
		String outFileName = DB_PATH + DB_NAME;
		OutputStream mOutput = new FileOutputStream(outFileName);
		byte[] mBuffer = new byte[1024];
		int mLength;
		while ((mLength = mInput.read(mBuffer)) > 0) {
			mOutput.write(mBuffer, 0, mLength);
		}
		mOutput.flush();
		mOutput.close();
		mInput.close();
	}

	public boolean openDataBase() throws SQLException {
		String mPath = DB_PATH + DB_NAME;
		mDataBase = SQLiteDatabase.openDatabase(mPath, null,
				SQLiteDatabase.CREATE_IF_NECESSARY);
		return mDataBase != null;
	}

	@Override
	public synchronized void close() {
		if (mDataBase != null)
			mDataBase.close();
		super.close();
	}

	@Override
	public void onCreate(SQLiteDatabase arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	// dokter
	public ArrayList<DokterData> getDataDokter() {
		Cursor cur = null;
		ArrayList<DokterData> drdata = new ArrayList<DokterData>();
		try {
			cur = mDataBase.rawQuery("SELECT * from dokter", null);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("DEBE ERROR", e.toString());
		}

		cur.moveToFirst();
		if (!cur.isAfterLast()) {
			do {
				drdata.add(new DokterData(cur.getString(1), cur.getString(2),
						cur.getString(3)));
			} while (cur.moveToNext());
		}
		return drdata;
	}

	public ArrayList<DokterData> setDataDokter(String json) {
		try {

			JSONObject jObject = new JSONObject(json);
			String status = jObject.getString("success");
			Log.d("status", status);
			if (status.equals("1")) {
				JSONArray dokterArray = jObject.getJSONArray("data");

				for (int i = 0; i < dokterArray.length(); i++) {
					JSONObject item = dokterArray.getJSONObject(i);
					String nama = item.getString("nama");
					String ahli = item.getString("ahli");
					String foto = item.getString("foto");

					ContentValues cval = new ContentValues();
					cval.put("foto", foto);
					cval.put("ahli", ahli);
					cval.put("nama", nama);

					mDataBase.insert("dokter", null, cval);
				}
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return this.getDataDokter();
	}

	// jadwal
	public ArrayList<JadwalData> getJadwalDokter() {
		Cursor cur = null;
		ArrayList<JadwalData> jadwalData = new ArrayList<JadwalData>();
		try {
			cur = mDataBase.rawQuery("SELECT * from jadwal", null);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("DEBE ERROR", e.toString());
		}

		String hari = "";

		cur.moveToFirst();
		if (!cur.isAfterLast()) {
			do {
				if (!(hari.equals(cur.getString(1)))) {
					hari = cur.getString(1);

					JadwalData dHari = new JadwalData();
					dHari.setHari(hari);
					dHari.setTipe("hari");
					jadwalData.add(dHari);

					jadwalData.add(new JadwalData(cur.getString(2), cur
							.getString(3), cur.getString(4), cur.getString(1)));
				} else {
					jadwalData.add(new JadwalData(cur.getString(2), cur
							.getString(3), cur.getString(4), cur.getString(1)));
				}
			} while (cur.moveToNext());
		}
		return jadwalData;
	}

	public ArrayList<JadwalData> setJadwalDokter(String data) {
		ArrayList<JadwalData> dd = new ArrayList<JadwalData>();

		mDataBase.delete("jadwal", null, null);

		try {

			JSONObject jObject = new JSONObject(data);

			for (String h : hari) {
				JSONArray jarray = jObject.getJSONArray(h);

				if (jarray.length() > 0) {
					JadwalData dHari = new JadwalData();
					dHari.setHari(h);
					dHari.setTipe("hari");
					dd.add(dHari);

					for (int i = 0; i < jarray.length(); i++) {
						JSONObject item = jarray.getJSONObject(i);
						dd.add(new JadwalData(item.getString("doctor"), item
								.getString("poli"), item.getString("jam"), h));

						ContentValues cval = new ContentValues();
						cval.put("hari", h);
						cval.put("doctor", item.getString("doctor"));
						cval.put("poli", item.getString("poli"));
						cval.put("jam", item.getString("jam"));
						cval.put("foto", "");

						mDataBase.insert("jadwal", null, cval);
					}
				}
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Log.d("ipon", dd.toString());

		return dd;
	}

	public ArrayList<RiwayatMedicalData> getRiwayatMedical() {

		Cursor cur = null;
		ArrayList<RiwayatMedicalData> data = new ArrayList<RiwayatMedicalData>();
		try {
			cur = mDataBase.rawQuery("SELECT * from riwayat", null);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("DEBE ERROR", e.toString());
		}

		cur.moveToFirst();
		if (!cur.isAfterLast()) {
			do {
				data.add(new RiwayatMedicalData(cur.getString(2), cur
						.getString(3), cur.getString(4), cur.getString(1)));
			} while (cur.moveToNext());
		}
		return data;
	}

	public ArrayList<RiwayatMedicalData> setRiwayatMedical(String data) {

		ArrayList<RiwayatMedicalData> dd = new ArrayList<RiwayatMedicalData>();
		mDataBase.delete("riwayat", null, null);

		try {

			JSONArray jarray = new JSONArray(data);

			if (jarray.length() > 0) {
				for (int i = 0; i < jarray.length(); i++) {
					JSONObject item = jarray.getJSONObject(i);
					dd.add(new RiwayatMedicalData(item.getString("date"), item
							.getString("doctor"), item.getString("poly"), item
							.getString("id")));

					ContentValues cval = new ContentValues();
					cval.put("server_id", item.getString("id"));
					cval.put("date", item.getString("date"));
					cval.put("doctor", item.getString("doctor"));
					cval.put("poly", item.getString("poly"));

					mDataBase.insert("riwayat", null, cval);
				}
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Log.d("ipon", dd.toString());
		return dd;
	}
}