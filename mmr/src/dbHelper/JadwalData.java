package dbHelper;

public class JadwalData {
	String dokter = "", poli = "", jam = "",hari="",tipe="data";

	public JadwalData() {
		// TODO Auto-generated constructor stub
	}

	public JadwalData(String _dokter, String _poli, String _jam,String _hari) {
		dokter = _dokter;
		poli = _poli;
		jam = _jam;
		hari = _hari;
	}

	public String getDokter() {
		return dokter;
	}

	public String getJam() {
		return jam;
	}

	public String getPoli() {
		return poli;
	}

	public void setDokter(String dokter) {
		this.dokter = dokter;
	}

	public void setJam(String jam) {
		this.jam = jam;
	}

	public void setPoli(String poli) {
		this.poli = poli;
	}
	
	public String getHari() {
		return hari;
	}
	
	public void setHari(String hari) {
		this.hari = hari;
	}

	public void setTipe(String string) {
		tipe = string;
	}
	
	public String getTipe() {
		return tipe;
	}
}
