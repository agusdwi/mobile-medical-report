package dbHelper;

import static helper.CommonUtilities.*;
import helper.CommonUtilities;
import helper.HTTPReq;
import helper.Token;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

public abstract class RiwayatMedicalAsync extends AsyncTask<String, Void, String>{

	private Context mainContext;
	private RelativeLayout loadingBar;
	ArrayList<RiwayatMedicalData> dd;
	String lastTime;
	String sReturn;
	SharedPreferences settings;
	private SharedPreferences.Editor editor;
	DataBaseHelper db;
	
	public RiwayatMedicalAsync(Context c, RelativeLayout rl) {
		mainContext = c;
		loadingBar = rl;
	}
	
	protected void onPreExecute() {
		loadingBar.setVisibility(View.VISIBLE);
	}
	
	public abstract void onResponseReceived(ArrayList<RiwayatMedicalData> dd2);

	protected String doInBackground(String... params) {
		db = DataBaseHelper.initDb(mainContext);
		
		settings = mainContext.getSharedPreferences(PREFS_NAME, 0);
		lastTime = settings.getString(RIWAYAT_LAST_TIME, "0");
		
		sReturn = "";
		String token = Token.getPrivateToken(mainContext);
		String url = CommonUtilities.SERVER_URL + "rekmed";
		
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("token", token));
		pairs.add(new BasicNameValuePair("last_time", lastTime));
		
		try {
			sReturn = HTTPReq.postRequest(url, pairs);
			Log.d("return",sReturn);
			composeJson(sReturn);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "";
	}

	protected void onCancelled() {

	}

	protected void onPostExecute(String asReturn) {
		loadingBar.setVisibility(View.GONE);
		onResponseReceived(dd);
	}
	
	private void composeJson(String msg){
		try {
			JSONObject jObject = new JSONObject(msg);
			
			String apiResult 	= jObject.getString("success").toString();
			if(apiResult.equals("1")){
				String serverLastTime = jObject.getString("last_time").toString();
				
				if(Float.parseFloat(serverLastTime) > Float.parseFloat(lastTime)){
					editor = settings.edit();
					editor.putString(RIWAYAT_LAST_TIME, serverLastTime);
					editor.commit();
					
					//updated data, stored to db
					String data = jObject.getString("data").toString();
					dd = db.setRiwayatMedical(data);
				}else{
					dd = null;
				}
				Log.d("last time", serverLastTime);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
}