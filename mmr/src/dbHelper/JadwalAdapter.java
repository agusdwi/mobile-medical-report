package dbHelper;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.mmr.R;

public class JadwalAdapter extends ArrayAdapter<JadwalData>{

	int resource;
	Context context;
	List<JadwalData> items;
	
	public JadwalAdapter(Context context, int resource, List<JadwalData> items) {
		super(context, resource, items);
		this.resource = resource;
		this.context = context;
		this.items = items;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LinearLayout ktView;

		JadwalData item = getItem(position);

		String dokter 	= item.getDokter(), 
				poli 	= item.getPoli(), 
				jam 	= item.getJam();
		
		if (convertView == null) {
			ktView = new LinearLayout(getContext());
			String inflater = Context.LAYOUT_INFLATER_SERVICE;
			LayoutInflater li;
			li = (LayoutInflater) getContext().getSystemService(inflater);
			li.inflate(resource, ktView, true);
		} else {
			ktView = (LinearLayout) convertView;
		}
		
		RelativeLayout r2 = (RelativeLayout) ktView.findViewById(R.id.relativeLayout2);
		RelativeLayout r3 = (RelativeLayout) ktView.findViewById(R.id.relativeLayout3);
		RelativeLayout r4 = (RelativeLayout) ktView.findViewById(R.id.relativeLayout4);
		
		TextView rowName 	= (TextView) ktView.findViewById(R.id.textView3);
		TextView rowJam 	= (TextView) ktView.findViewById(R.id.textView4);
		TextView rowPoli 	= (TextView) ktView.findViewById(R.id.poli);
		
		if(item.getTipe() == "data"){
			r2.setVisibility(View.VISIBLE);
			r3.setVisibility(View.VISIBLE);
			r4.setVisibility(View.GONE);
			
			rowName.setText(dokter);
			rowJam.setText(jam);
			rowPoli.setText(poli);
		}else{
			r2.setVisibility(View.GONE);
			r3.setVisibility(View.GONE);
			r4.setVisibility(View.VISIBLE);
			
			((TextView) ktView.findViewById(R.id.hari)).setText(item.getHari());
		}
		return ktView;
	}	
}