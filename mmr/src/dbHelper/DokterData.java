package dbHelper;

public class DokterData {

	String nama,ahli,foto;
	
	public DokterData() {
		// TODO Auto-generated constructor stub
	}
	
	public DokterData(String _nama,String _ahli,String _foto){
		nama = _nama;
		ahli = _ahli;
		foto = _foto;
	}
	
	public void setNama(String nama) {
		this.nama = nama;
	}
	
	public void setAhli(String ahli) {
		this.ahli = ahli;
	}
	
	public void setFoto(String foto) {
		this.foto = foto;
	}
	
	public String getNama() {
		return nama;
	}
	
	public String getAhli() {
		return ahli;
	}
	
	public String getFoto() {
		return foto;
	}
}