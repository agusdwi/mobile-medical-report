package dbHelper;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.mmr.R;

public class DokterAdapter extends ArrayAdapter<DokterData>{

	int resource;
	Context context;
	List<DokterData> items;
	
	public DokterAdapter(Context context, int resource, List<DokterData> items) {
		super(context, resource, items);
		this.resource = resource;
		this.context = context;
		this.items = items;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LinearLayout ktView;

		DokterData item = getItem(position);

		String nama 	= item.getNama();
		String ahli 	= item.getAhli();

		if (convertView == null) {
			ktView = new LinearLayout(getContext());
			String inflater = Context.LAYOUT_INFLATER_SERVICE;
			LayoutInflater li;
			li = (LayoutInflater) getContext().getSystemService(inflater);
			li.inflate(resource, ktView, true);
		} else {
			ktView = (LinearLayout) convertView;
		}
		
		TextView rowName 	= (TextView) ktView.findViewById(R.id.textView3);
		TextView rowAlamat 	= (TextView) ktView.findViewById(R.id.textView4);
		
		rowName.setText(nama);
		rowAlamat.setText(ahli);
        
		return ktView;
	}	
}