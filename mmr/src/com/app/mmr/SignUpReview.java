package com.app.mmr;

import helper.CheckActiveAsync;
import helper.RegisterAsync;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import static helper.CommonUtilities.*;

public class SignUpReview extends Activity {

	String errorCode,rekmed;
	private SharedPreferences settings;
	private SharedPreferences.Editor editor;
	Button btAct;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sign_up_review);
		
		btAct = (Button) findViewById(R.id.button1);
		
		Intent i = getIntent();
		errorCode 	= i.getStringExtra("error_code");
		rekmed 		= i.getStringExtra("rekmed");
		
		if (errorCode.equals("0")){
			((TextView) findViewById(R.id.textView1))
					.setText("Maaf nomer rekam medis "+rekmed+" yang anda masukkan tidak ada dalam database, silahkan ulangi");
			btAct.setText("COBA LAGI");
		}else if (errorCode.equals("1")){
			
			Toast.makeText(
					getApplicationContext(),
					"Anda telah terdaftar, tetapi belum mendapatkan persetujuan dari Rumah Sakit, silahkan menghubungi pusat informasi",
					Toast.LENGTH_SHORT).show();
			
			settings = getSharedPreferences(PREFS_NAME, 0);
			editor = settings.edit();
			editor.putString(PREFS_LOGIN_STATE, STATE_PENDING_APPROVAL);
			editor.putString(PREFS_REKMED, rekmed);
			editor.commit();
			
			Intent intent = new Intent(this, SplashScreen.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		}else if (errorCode.equals("2")){
			((TextView) findViewById(R.id.textView1))
					.setText("Anda telah terdaftar di sistem, silahkan login");
			btAct.setText("LOGIN SEKARANG");
		}else{
			((TextView) findViewById(R.id.textView1))
			.setText("Akun anda di blok, silahkan hubungi administrasi rumah sakit");
			btAct.setText("KELUAR");
		}
	}
	
	public void goBenar(View v){
		onBackPressed();
	}
	
	@Override
	public void onBackPressed() {
		if (errorCode.equals("0")){
			super.onBackPressed();
		}else if (errorCode.equals("3")){
			Intent intent = new Intent(getApplicationContext(), Login.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.putExtra("EXIT", true);
			startActivity(intent);
		}else{
			Intent i = new Intent(this, Login.class);
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
		}
	}
}