package com.app.mmr;

import helper.InitDataLoginAsync;
import helper.PasswordAsync;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static helper.CommonUtilities.*;

public class SignUpApproval extends Activity {

	private SharedPreferences settings;
	private SharedPreferences.Editor editor;
	private Context c;
	private Activity act;
	private JSONObject jObject;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sign_up_approval);
		
		act = getParent();
		c = this;
		
		settings 		= getSharedPreferences(PREFS_NAME, 0);
		editor = settings.edit();
		
		prosesInitData();
	}
	
	private void prosesInitData(){
		String rekmed = settings.getString(PREFS_REKMED, "0");
		InitDataLoginAsync R1 = new InitDataLoginAsync(c, act) {
			@Override
			public void onResponseReceived(String result) {
				try {
					
					jObject 			= new JSONObject(result);
					String apiResult 	= jObject.getString("success").toString();
					if(apiResult.equals("0")){
						Toast.makeText(getApplicationContext(), "Server error, please re launch apps", Toast.LENGTH_SHORT).show();
					}else{
						JSONObject js = jObject.getJSONObject("message");

						((TextView) findViewById(R.id.NoRekmed)).setText(js.getString("no_rekmed").toString());
						((TextView) findViewById(R.id.JP)).setText(js.getString("jenis_kelamin").toString());
						((TextView) findViewById(R.id.Nama)).setText(js.getString("nama").toString());
						((TextView) findViewById(R.id.Alamat)).setText(js.getString("alamat").toString());

						editor.putString(PREFS_INIT_DATA, result);
						editor.putString(PREFS_QRCODE, js.getString("qrcode").toString());
						editor.putString(PREFS_ALAMAT, js.getString("alamat").toString());
						editor.putString(PREFS_NAMA_PASIEN, js.getString("nama").toString());
						editor.commit();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				
			}
		};
		R1.execute(rekmed);	
	}
	
	public void goClick(View v){
		EditText ed1 = (EditText) findViewById(R.id.edPwd1);
		EditText ed2 = (EditText) findViewById(R.id.edPwd2);
		String rekmed = settings.getString(PREFS_REKMED, "0");
		
		if(ed1.getText().toString().equals(ed2.getText().toString())){
			if(ed1.getText().toString().length() < 6){
				Toast.makeText(getApplicationContext(), "password minimal 6 karakter", Toast.LENGTH_SHORT).show();
				ed1.requestFocus();
			}else{
				PasswordAsync P = new PasswordAsync(c, act) {
					@Override
					public void onResponseReceived(String result) {
						try {
							jObject 			= new JSONObject(result);
							String apiResult 	= jObject.getString("success").toString();
							String token 		= jObject.getString("token").toString();
							if(apiResult.equals("1")){
								editor.putString(PREFS_LOGIN_STATE, STATE_ALREADY_LOGIN);
								editor.putString(PREFS_TOKEN, token);
								editor.commit();
								Toast.makeText(getApplicationContext(), "Selamat anda berhasil login", Toast.LENGTH_SHORT).show();
								startActivity(new Intent(SignUpApproval.this, Main.class));
								finish();
							}else if(apiResult.equals("0")){	
								Toast.makeText(getApplicationContext(), "Server error, please try again", Toast.LENGTH_SHORT).show();
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				};
				P.execute(rekmed,ed1.getText().toString());
			}
		}else{
			ed2.requestFocus();
			Toast.makeText(getApplicationContext(), "password tidak sama, silahkan ulangi", Toast.LENGTH_SHORT).show();
		}
	}
}