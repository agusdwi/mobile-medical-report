package com.app.mmr;

import static helper.CommonUtilities.PREFS_LOGIN_STATE;
import static helper.CommonUtilities.PREFS_NAME;
import static helper.CommonUtilities.PREFS_REGID;
import static helper.CommonUtilities.PREFS_REKMED;
import static helper.CommonUtilities.SENDER_ID;
import static helper.CommonUtilities.STATE_PENDING_APPROVAL;
import helper.RegisterAsync;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;

public class SignUp extends Activity {

	private Context c;
	private JSONObject jObject;
	private SharedPreferences settings;
	private SharedPreferences.Editor editor;
	String regID = "";
	Button btSignUp;
	int i = 0;
	String noRekmed;
	ProgressDialog pd;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sign_up);
		
		//start init GCM
		checkNotNull(SENDER_ID, "SENDER_ID");
		GCMRegistrar.checkDevice(this);
		GCMRegistrar.checkManifest(this);
		regID = GCMRegistrar.getRegistrationId(this);
		
		TextView tv = (TextView)findViewById(R.id.textView1);
		tv.setText(Html.fromHtml(getString(R.string.app_sign_up)));
		
		c = this;
		btSignUp = (Button) findViewById(R.id.button1);
		regGCM();
		settings = getSharedPreferences(PREFS_NAME, 0);
		regID = settings.getString(PREFS_REGID, "");
		
		pd = new ProgressDialog(c);
		pd.setMessage("Sending data...");
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		i=0;
	}
	
	public void goRegister(View v){
		if(!pd.isShowing())
			pd.show();
		pd.setCancelable(false);	
		
		final View vv = v;
		
		noRekmed = ((EditText) findViewById(R.id.edNoRekmed)).getText().toString();
		Log.d("halo", "haloooo ==> "+Integer.toString(i));
		v.postDelayed(new Runnable() {
	        @Override
	        public void run() {
	        	i++;
	        	if(i < 10){
	        		regID = settings.getString(PREFS_REGID, "");
	        		if(regID.equals("")){
	        			i++;
	        			goRegister(vv);
	        		}else{
	        			pd.dismiss();
	        			i=11;
	        			prosesDaftar(noRekmed,regID,pd);
	        		}
	        	}else{
	        		pd.dismiss();
	        		Toast.makeText(getApplicationContext(), "server error, please try again", Toast.LENGTH_SHORT).show();
	        	}
	        }
	    }, 1000L);
	}
	
	private void prosesDaftar(final String rekmed, String regID,ProgressDialog pd){
		i=0;
		RegisterAsync R = new RegisterAsync(c,pd) {
			@Override
			public void onResponseReceived(String result) {
				try {
					jObject 			= new JSONObject(result);
					String apiResult 	= jObject.getString("success").toString();
					if(apiResult.equals("0")){
						Intent i = new Intent(c, SignUpReview.class);
						i.putExtra("error_code", jObject.getString("error_code").toString());
						i.putExtra("rekmed", rekmed);
						startActivity(i);
					}else if(apiResult.equals("1")){	
						settings = getSharedPreferences(PREFS_NAME, 0);
						editor = settings.edit();
						editor.putString(PREFS_LOGIN_STATE, STATE_PENDING_APPROVAL);
						editor.putString(PREFS_REKMED, rekmed);
						editor.commit();
						startActivity(new Intent(c, SignUpPending.class));
						finish();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		};
		R.execute(rekmed,regID);	
	}
	
	private void regGCM(){
		new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
            	GCMRegistrar.register(c, SENDER_ID);
            	regID = GCMRegistrar.getRegistrationId(c);
                return "";
            }
            @Override
            protected void onPostExecute(String msg) {
            	//prosesDaftar(noRekmed,regID);	
            }
        }.execute(null, null, null);
	}
	
	private void checkNotNull(Object reference, String name) {
		if (reference == null) {
			throw new NullPointerException("error config name");
		}
	}
}