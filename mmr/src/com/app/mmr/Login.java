package com.app.mmr;

import static helper.CommonUtilities.*;
import helper.LoginAsync;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.google.android.gcm.GCMRegistrar;

public class Login extends SherlockActivity {
	
	private Context c;
	String regID = "";
	ProgressDialog pd;
	int i;
	private SharedPreferences settings;
	private SharedPreferences.Editor editor;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if (getIntent().getBooleanExtra("EXIT", false)) {
		    finish();
		}
		c = this;
		getGCM();
		setContentView(R.layout.login); 
		
		settings = getSharedPreferences(PREFS_NAME, 0);
		regID = settings.getString(PREFS_REGID, "");
		pd = new ProgressDialog(c);
		pd.setMessage("Sending data...");
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		i=0;
	}
	
	public void goLogin(View v){
		if(!pd.isShowing())
			pd.show();
		pd.setCancelable(false);	
		final View vv = v;
		v.postDelayed(new Runnable() {
	        @Override
	        public void run() {
	        	i++;
	        	if(i < 10){
	        		regID = settings.getString(PREFS_REGID, "");
	        		if(regID.equals("")){
	        			i++;
	        			goLogin(vv);
	        		}else{
	        			pd.dismiss();
	        			i=11;
	        			final String noRekmed = ((EditText) findViewById(R.id.editText1)).getText().toString();
	        			String pwd 		= ((EditText) findViewById(R.id.editText2)).getText().toString();
	        			
	        			new LoginAsync(c) {
	        				@Override
	        				public void onResponseReceived(String result) {
	        					JSONObject jObject;
								try {
									jObject = new JSONObject(result);
									String apiResult 	= jObject.getString("success").toString();
									if(apiResult.equals("0")){
										Toast.makeText(getApplicationContext(), "Maaf password / no rekmed anda salah", Toast.LENGTH_SHORT).show();
									}else{
										editor = settings.edit();
										editor.putString(PREFS_LOGIN_STATE, STATE_ALREADY_LOGIN);
										editor.putString(PREFS_REKMED, noRekmed);
										editor.putString(PREFS_TOKEN, jObject.getString("token").toString());
										editor.putString(PREFS_QRCODE, jObject.getString("qrcode").toString());
										editor.putString(PREFS_ALAMAT, jObject.getString("alamat").toString());
										editor.putString(PREFS_NAMA_PASIEN, jObject.getString("nama").toString());
										editor.putString(PREFS_PROFPIC, jObject.getString("foto").toString());
										editor.commit();
										startActivity(new Intent(c, Main.class));
										finish();
									}
									
								} catch (JSONException e) {
									e.printStackTrace();
								}
	        				}
	        			}.execute(noRekmed,pwd,regID);
	        		}
	        	}else{
	        		pd.dismiss();
	        		Toast.makeText(getApplicationContext(), "server error, please try again", Toast.LENGTH_SHORT).show();
	        	}
	        }
	    }, 1000L);
	}
	
	public void goSignup(View v){
		Intent intent = new Intent(this, SignUp.class);
		startActivity(intent);
	}
	
	public void getGCM(){
		checkNotNull(SENDER_ID, "SENDER_ID");
		GCMRegistrar.checkDevice(this);
		GCMRegistrar.checkManifest(this);
		regID = GCMRegistrar.getRegistrationId(this);
		
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				GCMRegistrar.register(c, SENDER_ID);
				regID = GCMRegistrar.getRegistrationId(c);
				return "";
			}
			@Override
			protected void onPostExecute(String msg) {}
		}.execute(null, null, null);
	}
	
	private void checkNotNull(Object reference, String name) {
		if (reference == null) {
			throw new NullPointerException("error config name");
		}
	}
}