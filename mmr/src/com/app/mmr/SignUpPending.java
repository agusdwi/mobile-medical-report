package com.app.mmr;

import helper.CheckActiveAsync;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import static helper.CommonUtilities.*;

public class SignUpPending extends Activity {

	AsyncTask<Void, Void, Void> mRegisterTask;
	private SharedPreferences settings;
	private SharedPreferences.Editor editor;
	String rekmed;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sign_up_pending);
		
		settings 	= getSharedPreferences(PREFS_NAME, 0);
		rekmed		= settings.getString(PREFS_REKMED, "0");
		
		checkIsActive();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		
		Intent intent = new Intent(getApplicationContext(), Login.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra("EXIT", true);
		startActivity(intent);
		finish();
	}
	
	public void checkIsActive(){
		CheckActiveAsync R = new CheckActiveAsync(this, getParent()) {
			@Override
			public void onResponseReceived(String result) {
				try {
					JSONObject jObject 	= new JSONObject(result);
					String apiResult 	= jObject.getString("success").toString();
					String token 		= jObject.getString("token").toString();
					if(apiResult.equals("1")){
						editor = settings.edit();
						editor.putString(PREFS_LOGIN_STATE, STATE_APPROVAL_INIT);
						editor.putString(PREFS_TOKEN, token);
						editor.commit();
						
						Intent intent = new Intent(getApplicationContext(), SignUpApproval.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						finish();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		};
		R.execute(rekmed);
	}
}
