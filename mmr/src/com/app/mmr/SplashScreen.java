package com.app.mmr;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import static helper.CommonUtilities.*;

public class SplashScreen extends Activity {
	private SharedPreferences settings;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		
		settings = getSharedPreferences(PREFS_NAME, 0);

		String state = settings.getString(PREFS_LOGIN_STATE, "0"); 
		
		if (state.equals(STATE_NOT_LOGIN)) {
			setContentView(R.layout.splash_screen);
			Thread timer = new Thread() {
				public void run() {
					try {
						sleep(3000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					} finally {
						Intent i = new Intent(SplashScreen.this, Login.class);
						startActivity(i);
						finish();
					}
				}
			};
			timer.start();
		} else if(state.equals(STATE_PENDING_APPROVAL)){
			startActivity(new Intent(this, SignUpPending.class));
			finish();
		} else if(state.equals(STATE_APPROVAL_INIT)){
			startActivity(new Intent(this, SignUpApproval.class));
			finish();
		} else {
			startActivity(new Intent(this, Main.class));
			finish();
		}
	}
}