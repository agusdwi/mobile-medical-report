package com.app.mmr;

import static helper.CommonUtilities.PREFS_NAMA_PASIEN;
import static helper.CommonUtilities.PREFS_NAME;
import static helper.CommonUtilities.PREFS_PROFPIC;
import static helper.CommonUtilities.PREFS_REKMED;
import helper.LoadImageAsync;

import java.util.ArrayList;

import adapter.SlideMenuAdapter;
import adapter.SlideMenuClass;
import android.R.bool;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.actionbarsherlock.view.Window;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.menu.dokter.DokterFragment;
import com.menu.home.HomeFragment;
import com.menu.info.InfoFragment;
import com.menu.jadwal.JadwalFragment;
import com.menu.layanan.LayananFragment;
import com.menu.riwayat.RiwayatFragment;


public class Main extends SherlockFragmentActivity{

	SlidingMenu menu;
	private ArrayList<SlideMenuClass> itemMenu;
	private SlideMenuAdapter adapterMenu;
	ListView lvSlideMenu;
	ImageView ivAvatar;
	SharedPreferences settings;
	
	private Fragment mContent;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		populateSlideMenu();
		
		switchContent(new HomeFragment());
		
		getSherlock().setProgressBarIndeterminateVisibility(true);
	}
	
	@Override
	public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			menu.toggle();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		if (menu.isMenuShowing())
			menu.toggle();
		else {
			super.onBackPressed();
		}
	}
	
	private void populateSlideMenu(){
		menu = new SlidingMenu(this);
		menu.setMode(SlidingMenu.LEFT);
		menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		menu.setShadowWidthRes(R.dimen.shadow_width);
		menu.setShadowDrawable(R.drawable.shadow);
		menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		menu.setFadeDegree(0.35f);
		menu.attachToActivity(this, SlidingMenu.SLIDING_WINDOW);
		menu.setMenu(R.layout.menu);

		lvSlideMenu = (ListView) findViewById(R.id.lvSlideMenu);

		itemMenu = new ArrayList<SlideMenuClass>();
		adapterMenu = new SlideMenuAdapter(getApplicationContext(),
				R.layout.slide_menu_item, itemMenu);

		adapterMenu.add(new SlideMenuClass("Home", R.drawable.m_home, true, 1));
		adapterMenu.add(new SlideMenuClass("Riwayat Medis", R.drawable.m_riwayat, true, 2));
		adapterMenu.add(new SlideMenuClass("Jadwal Dokter", R.drawable.m_jadwal, true, 2));
		adapterMenu.add(new SlideMenuClass("Data Dokter", R.drawable.m_dokter, true, 2));
		adapterMenu.add(new SlideMenuClass("Pendaftaran Layanan", R.drawable.m_layanan, true, 2));
		adapterMenu.add(new SlideMenuClass("Notifikasi", R.drawable.m_notif, true, 2));
		adapterMenu.add(new SlideMenuClass("Informasi", R.drawable.m_info, true, 2));

		lvSlideMenu.setAdapter(adapterMenu);
		lvSlideMenu.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
				Fragment newContent = null;
				switch (arg2) {
				case 0:
					newContent = new HomeFragment();
					break;
				case 1:
					newContent = new RiwayatFragment();
					break;
				case 2:
					newContent = new JadwalFragment();
					break;
				case 3:
					newContent = new DokterFragment();
					break;
				case 4:
					newContent = new LayananFragment();
					break;
				case 5:
					newContent = new InfoFragment();
					break;
				case 6:
					newContent = new InfoFragment();
					break;
				}
				if (newContent != null)
					switchContent(newContent);
			}
		});
		
		settings = getSharedPreferences(PREFS_NAME, 0);
		((TextView) findViewById(R.id.topName)).setText(settings.getString(PREFS_NAMA_PASIEN, ""));
		((TextView) findViewById(R.id.topRekmed)).setText(settings.getString(PREFS_REKMED, ""));
		
		ivAvatar = (ImageView) findViewById(R.id.topImage);
		
		String img = settings.getString(PREFS_PROFPIC, "");
		if(!img.equals("")){
			LoadImageAsync li = new LoadImageAsync(this,ivAvatar);
			li.execute(img);
		}
	}
	
	public void loadImage(){
		ivAvatar = (ImageView) findViewById(R.id.topImage);

		String img = settings.getString(PREFS_PROFPIC, "");
		if (!img.equals("")) {
			LoadImageAsync li = new LoadImageAsync(this, ivAvatar);
			li.execute(img);
		}
	}
	
	public void switchContent(Fragment fragment) {
		mContent = fragment;
			getSupportFragmentManager()
			.beginTransaction()
			.replace(R.id.content_frame, fragment)
			.commit();
		menu.showContent();
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		getSupportFragmentManager().putFragment(outState, "mContent", mContent);
	}
}
