package com.app.mmr;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import static helper.CommonUtilities.*;

import com.google.android.gcm.GCMBaseIntentService;

public class GCMIntentService extends GCMBaseIntentService {

	private SharedPreferences settings;
	private SharedPreferences.Editor editor;
	
	public GCMIntentService() {
		super(SENDER_ID);
	}

	private static final String TAG = "===GCMIntentService===";

	@Override
	protected void onRegistered(Context arg0, String registrationId) {
		Log.i(TAG, "Device registered: regId = " + registrationId);
		
		settings	 	= getSharedPreferences(PREFS_NAME, 0);
		editor			= settings.edit();
		editor.putString(PREFS_REGID, registrationId);
		editor.commit();
	}

	@Override
	protected void onUnregistered(Context arg0, String arg1) {
		Log.i(TAG, "unregistered = " + arg1);
	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		Log.i(TAG, "new message= ");
		
		String notif_type 	= intent.getExtras().getString("notif_type");
		
		if(notif_type.equals("activation_success")){
			String message 	= intent.getExtras().getString("message");
			String rekmed 	= intent.getExtras().getString("rekmed");
			String token 	= intent.getExtras().getString("token");
			String qrcode 	= intent.getExtras().getString("qrcode");
			
			settings = getSharedPreferences(PREFS_NAME, 0);
			String state = settings.getString(PREFS_REKMED, "0");
			
			editor = settings.edit();
			editor.putString(PREFS_TOKEN, token);
			editor.putString(PREFS_LOGIN_STATE, STATE_APPROVAL_INIT);
			editor.putString(PREFS_QRCODE, qrcode);
			editor.commit();
			
			if(state.equals(rekmed)){
				generateNotification(context, message);
			}else{
				generateNotification(context, "maaf nomer rekam medis anda tidak ada");
			}
		}else if(notif_type.equals("logout")){
			getSharedPreferences(PREFS_NAME, 0).edit().clear().commit();
			generateNotification(context, "Anda telah login di tempat lain, akun anda di logout");
		}
	}

	@Override
	protected void onError(Context arg0, String errorId) {
		Log.i(TAG, "Received error: " + errorId);
	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		return super.onRecoverableError(context, errorId);
	}

	/**
	 * Issues a notification to inform the user that server has sent a message.
	 */
	private static void generateNotification(Context context, String message) {
		int icon = R.drawable.ic_launcher;
		long when = System.currentTimeMillis();
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(icon, message, when);
		String title = "Mobile Medical Care";
		Intent notificationIntent = new Intent(context, SplashScreen.class);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent intent = PendingIntent.getActivity(context, 0,
				notificationIntent, 0);
		notification.setLatestEventInfo(context, title, message, intent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notificationManager.notify(0, notification);
	}
}
