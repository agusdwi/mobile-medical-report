package com.menu.info;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.app.mmr.R;

public class notifFragment extends SherlockFragment{

	private FragmentActivity parent;

	public notifFragment() {
		setRetainInstance(true);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		parent = getActivity();
		parent.setTitle(R.string.tfr_info);
		
		TextView tv = (TextView) getView().findViewById(R.id.textView1);
		tv.setText(Html.fromHtml(getString(R.string.app_info)));
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.info_fragment, container, false);
	}	
}