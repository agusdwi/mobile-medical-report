package com.menu.riwayat;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.app.mmr.R;

import dbHelper.DataBaseHelper;
import dbHelper.RiwayatMedicalAdapter;
import dbHelper.RiwayatMedicalAsync;
import dbHelper.RiwayatMedicalData;

public class RiwayatFragment extends SherlockFragment {

	private FragmentActivity parent;
	private ListView lv;

	public RiwayatFragment() {
		setRetainInstance(true);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		parent = getActivity();
		parent.setTitle(R.string.tfr_riwayat);
		
		lv = (ListView)parent.findViewById(R.id.lvRiwayat);
		
		DataBaseHelper db = DataBaseHelper.initDb(parent);
		RiwayatMedicalAdapter ddAdapter = new RiwayatMedicalAdapter(parent, R.layout.item_riwayat, db.getRiwayatMedical());
		lv.setAdapter(ddAdapter);
		
		RelativeLayout loadingBar = (RelativeLayout) parent.findViewById(R.id.loadingBar);
		
		//async progress
		new RiwayatMedicalAsync(parent,loadingBar) {
			
			@Override
			public void onResponseReceived(ArrayList<RiwayatMedicalData> dd2) {
				if(dd2 != null){
					RiwayatMedicalAdapter ddAdapter2 = new RiwayatMedicalAdapter(parent, R.layout.item_riwayat, dd2);
					lv.setAdapter(ddAdapter2);
				}
			}
		}.execute("");
		
//		lv.setOnItemClickListener(new OnItemClickListener() {
//			@Override
//			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
//					long arg3) {
//				Intent intent = new Intent(parent, RiwayatDetail.class);
//				startActivity(intent);
//			}
//		});
		
		lv.setOnItemClickListener(new OnItemClickListener()
		{
		    @Override public void onItemClick(AdapterView<?> arg0, View arg1,int position, long arg3)
		    { 
		        Toast.makeText(parent, "Stop Clicking me", Toast.LENGTH_SHORT).show();
		    }
		});
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.riwayat_fragment, container, false);
	}
}