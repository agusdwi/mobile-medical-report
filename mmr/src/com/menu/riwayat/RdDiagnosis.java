package com.menu.riwayat;

import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;

public final class RdDiagnosis extends SherlockFragment {
	
	private static final String KEY_CONTENT = "TestFragment:Content";
	
    public static RdDiagnosis newInstance(String content) {
        RdDiagnosis fragment = new RdDiagnosis();
        return fragment;
    }
 
    private String mContent = "<b>diagnosa</b><br>- B41 Paracoccidioidomycosis <br>- J31 Chronic rhinitis, nasopharyngitis and pharyngitis <br><br><b>anamnesa</b><br>- Batuk disertai flu <br><br><b>pemeriksaan fisik</b><br>- Badan panas <br><br><b>pemeriksaan penunjang</b><br>- Terlalu banyak merokok <br><br>";
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
 
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        TextView text = new TextView(getActivity());
        text.setText(Html.fromHtml(mContent));
        text.setTextSize(14 * getResources().getDisplayMetrics().density);
        text.setPadding(20, 20, 20, 20);
 
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
        layout.setGravity(Gravity.CENTER);
        layout.addView(text);
 
        return layout;
    }
 
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_CONTENT, mContent);
    }
}
