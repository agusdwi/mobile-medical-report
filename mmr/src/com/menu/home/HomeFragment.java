package com.menu.home;

import static helper.CommonUtilities.*;
import helper.LoadImageAsync;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.app.mmr.Main;
import com.app.mmr.R;

public class HomeFragment extends SherlockFragment {
	ImageView ivAvatar, qrCode;
	private FragmentActivity parent;
	private SharedPreferences settings;

	public HomeFragment() {
		setRetainInstance(true);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		parent = getActivity();
		parent.setTitle(R.string.tfr_profil);
		settings = parent.getSharedPreferences(PREFS_NAME, 0);
		
		qrCode = (ImageView) parent.findViewById(R.id.qrcode);
		qrCode.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(parent, Qrcode.class);
				startActivity(intent);
			}
		});
		ivAvatar = (ImageView) parent.findViewById(R.id.ivAvatar);
		ivAvatar.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(parent, UploadImage.class);
				startActivity(intent);
			}
		});
		
		((TextView) parent.findViewById(R.id.textView7)).setText(settings.getString(PREFS_NAMA_PASIEN, ""));
		((TextView) parent.findViewById(R.id.textView8)).setText(settings.getString(PREFS_REKMED, ""));
		((TextView) parent.findViewById(R.id.textView9)).setText(settings.getString(PREFS_ALAMAT, ""));
		
		loadImage();		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.home_fragment, container, false);
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		loadImage();
		((Main)getActivity()).loadImage();
	}
	
	public void loadImage(){
		String profpic 	= settings.getString(PREFS_PROFPIC, "");		
		if(profpic.equals("")){
			ivAvatar.setImageResource(R.drawable.def_avatar);
		}else{
			LoadImageAsync li = new LoadImageAsync(parent,ivAvatar);
			li.execute(profpic);
		}
		
		String qrcode 	= settings.getString(PREFS_QRCODE, "");
		if(!qrcode.equals("")){
			LoadImageAsync li = new LoadImageAsync(parent,qrCode);
			li.execute(qrcode);
		}
	}
}