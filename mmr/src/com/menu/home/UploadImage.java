package com.menu.home;

import helper.LoadImageAsync;
import helper.UploadAsync;

import java.io.File;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.mmr.R;
import com.app.mmr.SplashScreen;
import static helper.CommonUtilities.*;

public class UploadImage extends Activity {

	private static int SELECT_FILE1 = 1;
	private static int STATE_UPLOAD = 2;
	private static int STATE_FINISH = 3;
	int state = STATE_UPLOAD;
	public String selectedPath1, selectedPath2;
	public String serverResponseMessage;
	ImageView ivAvatarUpload;
	private Context c;
	private Activity act;
	private SharedPreferences settings;
	private SharedPreferences.Editor editor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_upload_image);

		ivAvatarUpload = (ImageView) findViewById(R.id.ivAvatarUpload);

		act = getParent();
		c = this;

		settings = getSharedPreferences(PREFS_NAME, 0);
		editor = settings.edit();
	}

	public void goGallery(View v) {

//		Intent intent = new Intent();
//		intent.addCategory(Intent.CATEGORY_OPENABLE);
//		intent.setType("image/jpeg");
//		intent.setAction(Intent.ACTION_GET_CONTENT);
//		startActivityForResult(
//				Intent.createChooser(intent, "Select file to upload "),
//				SELECT_FILE1);
		
		Intent i = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

		startActivityForResult(i, SELECT_FILE1);

	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			Uri selectedImageUri = data.getData();
			if (requestCode == SELECT_FILE1) {
				selectedPath1 = getPath(selectedImageUri);
				if(selectedPath1 == null){
					Toast.makeText(getApplicationContext(), "Please select foto from gallery", Toast.LENGTH_SHORT).show();
				}else{
					File imgFile = new File(selectedPath1);
					if (imgFile.exists()) {
						Bitmap myBitmap = BitmapFactory.decodeFile(imgFile
								.getAbsolutePath());
//						ivAvatarUpload.setImageBitmap(myBitmap);
						ivAvatarUpload.setImageURI(selectedImageUri);
					}else{
						ivAvatarUpload.setImageURI(selectedImageUri);
					}
				}
			}
		}
	}

	public String getPath(Uri uri) {

		if( uri == null ) {
            return null;
        }
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if( cursor != null ){
            int column_index = cursor
            .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
	}

	public void goUpload(View v) {
		if (state == STATE_UPLOAD) {
			UploadAsync R1 = new UploadAsync(c) {
				@Override
				public void onResponseReceived(String result) {

					try {
						JSONObject jObject = new JSONObject(result);
						String res = jObject.getString("success").toString();

						if (res.equals("1")) {
							String img = jObject.getString("filename").toString();
							LoadImageAsync li = new LoadImageAsync(c,
									ivAvatarUpload);
							li.execute(img);
							state = STATE_FINISH;
							editor.putString(PREFS_PROFPIC, img);
							editor.commit();

							((Button) findViewById(R.id.button1))
									.setText("Selesai");
							
							Toast.makeText(
									getApplicationContext(),
									"Upload berhasil",
									Toast.LENGTH_SHORT).show();
						} else {
							Toast.makeText(
									getApplicationContext(),
									"Upload error, silahkan ulangi lagi",
									Toast.LENGTH_SHORT).show();
							ivAvatarUpload.setImageResource(R.drawable.def_search);
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			};
			String token = settings.getString(PREFS_TOKEN, "0");
			R1.execute(selectedPath1,token);
		}else{
			onBackPressed();
		}
	}
}
