package com.menu.home;

import helper.LoadImageAsync;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.mmr.R;
import com.app.mmr.SplashScreen;
import static helper.CommonUtilities.*;

public class Qrcode extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_qrcode);
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		String qrcode 	= settings.getString(PREFS_QRCODE, "");
		ImageView qrCode = (ImageView) findViewById(R.id.imageView1);
		if(!qrcode.equals("")){
			LoadImageAsync li = new LoadImageAsync(this,qrCode);
			li.execute(qrcode);
		}
		String frmt = "No Rekam Medis : \n";
		String rm   = getSharedPreferences(PREFS_NAME, 0).getString(PREFS_REKMED, "");
		((TextView) findViewById(R.id.textView1)).setText(frmt+rm);
	}
}