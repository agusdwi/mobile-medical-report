package com.menu.jadwal;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.actionbarsherlock.app.SherlockFragment;
import com.app.mmr.R;

import dbHelper.DataBaseHelper;
import dbHelper.JadwalAdapter;
import dbHelper.JadwalAsync;
import dbHelper.JadwalData;

public class JadwalFragment extends SherlockFragment{

	private FragmentActivity parent;
	private ListView lv;
	
	public JadwalFragment() {
		setRetainInstance(true);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState); 

		parent = getActivity();
		parent.setTitle(R.string.tfr_jadwal);
		
		lv = (ListView)parent.findViewById(R.id.lvJadwal);
		
		DataBaseHelper db = DataBaseHelper.initDb(parent);
		JadwalAdapter ddAdapter = new JadwalAdapter(parent, R.layout.item_jadwal, db.getJadwalDokter());
		lv.setAdapter(ddAdapter);
		
		RelativeLayout rl = (RelativeLayout) parent.findViewById(R.id.loadingBar);
		
		new JadwalAsync(parent,rl) {
			@Override
			public void onResponseReceived(ArrayList<JadwalData> sReturn) {
				
				if(sReturn != null){
					JadwalAdapter ddAdapter2 = new JadwalAdapter(parent, R.layout.item_jadwal, sReturn);
					lv.setAdapter(ddAdapter2);
				}
			}
		}.execute("");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.jadwal_fragment, container, false);
	}
}