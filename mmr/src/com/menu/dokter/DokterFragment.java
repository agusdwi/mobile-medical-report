package com.menu.dokter;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragment;
import com.app.mmr.R;

import dbHelper.DataBaseHelper;
import dbHelper.DokterAdapter;
import dbHelper.DokterAsync;
import dbHelper.DokterData;

public class DokterFragment extends SherlockFragment {
	private FragmentActivity parent;
	ListView lvRs;

	public DokterFragment() {
		setRetainInstance(true);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		parent = getActivity();
		parent.setTitle(R.string.tfr_dokter);
		
		lvRs = (ListView) parent.findViewById(R.id.listView1);
		
		new DokterAsync(parent) {
			
			@Override
			public void onResponseReceived(ArrayList<DokterData> sReturn) {
				// TODO Auto-generated method stub
				DokterAdapter ddAdapter = new DokterAdapter(parent, R.layout.item_dokter, sReturn);
				lvRs.setAdapter(ddAdapter);
			}
		}.execute("");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.dokter_fragment, container, false);
	}
}