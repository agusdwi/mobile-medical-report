package com.menu.layanan;

import helper.CommonUtilities;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.app.mmr.R;

public class LayananFragment extends SherlockFragment {

	Date selDate, today;
	TextView tvTanggal;

	private FragmentActivity parent;

	public LayananFragment() {
		setRetainInstance(true);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		parent = getActivity();
		parent.setTitle(R.string.tfr_layanan);

		selDate = today = new Date();
		tvTanggal = (TextView) parent.findViewById(R.id.tvTanggal);
		tvTanggal.setText(dateFormat(selDate));

		initButton();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.layanan_fragment, container, false);
	}

	public void initButton() {
		((Button) parent.findViewById(R.id.button2))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Calendar c = Calendar.getInstance();
						c.setTime(selDate);
						c.add(Calendar.DATE, 1);
						selDate = c.getTime();
						tvTanggal.setText(dateFormat(selDate));
					}
				});

		((Button) parent.findViewById(R.id.button3))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (today.equals(selDate)) {

						} else {
							Calendar c = Calendar.getInstance();
							c.setTime(selDate);
							c.add(Calendar.DATE, -1);
							selDate = c.getTime();
							tvTanggal.setText(dateFormat(selDate));
						}
					}
				});
	}

	private String dateFormat(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH)
				.format(date);
		dayOfWeek = CommonUtilities.indoDay(dayOfWeek);
		return dayOfWeek + ", " + sdf.format(date);
	}

	
}