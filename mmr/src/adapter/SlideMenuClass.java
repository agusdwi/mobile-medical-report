package adapter;

public class SlideMenuClass {
	String name;
	int id,icon;
	Boolean isRoom;
	
	public SlideMenuClass() {}
	
	public SlideMenuClass(String _name, int _icon, Boolean _isRoom, int _id) {
		name 	= _name;
		icon 	= _icon;
		isRoom 	= _isRoom;
		id 		= _id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setIcon(int icon) {
		this.icon = icon;
	}
	
	public int getIcon() {
		return icon;
	}
	
	public void setIsRoom(Boolean isRoom) {
		this.isRoom = isRoom;
	}
	
	public Boolean getIsRoom() {
		return isRoom;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
}