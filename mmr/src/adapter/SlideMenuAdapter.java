package adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.mmr.R;


public class SlideMenuAdapter extends ArrayAdapter<SlideMenuClass>{
	
	int resource;
	Context context;
	List<SlideMenuClass> items;
	
	public SlideMenuAdapter(Context context, int resource, List<SlideMenuClass> items) {
		super(context, resource, items);
		this.resource = resource;
		this.context = context;
		this.items = items;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LinearLayout ktView;

		SlideMenuClass item = getItem(position);

		String name 	= item.getName();
		int icon		= item.getIcon(); 

		if (convertView == null) {
			ktView = new LinearLayout(getContext());
			String inflater = Context.LAYOUT_INFLATER_SERVICE;
			LayoutInflater li;
			li = (LayoutInflater) getContext().getSystemService(inflater);
			li.inflate(resource, ktView, true);
		} else {
			ktView = (LinearLayout) convertView;
		}
		
		TextView rowName 	= (TextView) ktView.findViewById(R.id.row_title);
		ImageView rowIcon = (ImageView) ktView.findViewById(R.id.row_icons);
		
		rowName.setText(name);
		rowIcon.setImageResource(icon);
        
		return ktView;
	}
	
}
