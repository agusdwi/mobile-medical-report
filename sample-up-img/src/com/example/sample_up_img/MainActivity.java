package com.example.sample_up_img;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.HttpResponse;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private static int SELECT_FILE1 = 1;
	private static int SELECT_FILE2 = 2;
	public String selectedPath1,selectedPath2;
	public String serverResponseMessage;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	public void goGallery(View v){
		openGallery(SELECT_FILE1);
	}
	
	public void openGallery(int req_code){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select file to upload "), req_code);
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			Uri selectedImageUri = data.getData();
			if (requestCode == SELECT_FILE1) {
				selectedPath1 = getPath(selectedImageUri);
			}
			if (requestCode == SELECT_FILE2) {
				selectedPath2 = getPath(selectedImageUri);
			}
			((TextView) findViewById(R.id.tv)).setText("Selected File paths : "
					+ selectedPath1 + "," + selectedPath2);
			Toast.makeText(getApplicationContext(), "daftar", Toast.LENGTH_SHORT).show();
			getDatas g = new getDatas();
			g.execute("");
			uploadToServer(selectedPath1);
		}
	}
	
	public String getPath(Uri uri) {

		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}
	
	private class getDatas extends AsyncTask<String, Void, String> {

		private ProgressDialog Dialog = new ProgressDialog(MainActivity.this);
			
		@Override
		protected void onPreExecute() {
			Dialog.setMessage("Sending Data ...");
			Dialog.show();
		}


		@Override
		protected String doInBackground(String... urls) {
			String response = "";
			
			uploadToServer(selectedPath1);
			
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			Toast.makeText(getApplicationContext(), serverResponseMessage,
					Toast.LENGTH_SHORT).show();
			Dialog.dismiss();		
		}
	}
	
	public void uploadToServer(String s){
		HttpURLConnection connection = null;
		DataOutputStream outputStream = null;
		DataInputStream inputStream = null;

		String pathToOurFile = s;
		String urlServer = "http://bedicorporation.com/mmr/api/upload";
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary =  "*****";

		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1*1024*1024;

		try
		{
			FileInputStream fileInputStream = new FileInputStream(new File(pathToOurFile) );

			URL url = new URL(urlServer);
			connection = (HttpURLConnection) url.openConnection();

			// Allow Inputs & Outputs
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);

			// Enable POST method
			connection.setRequestMethod("POST");

			connection.setRequestProperty("Connection", "Keep-Alive");
			connection.setRequestProperty("Content-Type", "multipart/form-data;boundary="+boundary);

			outputStream = new DataOutputStream( connection.getOutputStream() );
			outputStream.writeBytes(twoHyphens + boundary + lineEnd);
			outputStream.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\"" + pathToOurFile +"\"" + lineEnd);
			outputStream.writeBytes(lineEnd);

			bytesAvailable = fileInputStream.available();
			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];

			// 	Read file
			bytesRead = fileInputStream.read(buffer, 0, bufferSize);

			while (bytesRead > 0)
			{
				outputStream.write(buffer, 0, bufferSize);
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			}

			outputStream.writeBytes(lineEnd);
			outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			// Responses from the server (code and message)
			int serverResponseCode = connection.getResponseCode();
			serverResponseMessage = connection.getResponseMessage();
			


			InputStream in = connection.getInputStream();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(in));
			StringBuilder str = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				str.append(line + "\n");
			}
			in.close();
			serverResponseMessage = str.toString();

			fileInputStream.close();
			outputStream.flush();
			outputStream.close();
		}
		catch (Exception ex){
		//Exception handling
		}
	}
}