package com.example.sample_dl_img;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends Activity {

	String a ;
	public String fp;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
	public void goDl(View v){
		getDatas g = new getDatas();
		g.execute("");
	}
	
	public void setGambar(String path){
		Bitmap bmp = BitmapFactory.decodeFile(path);
		((ImageView) findViewById(R.id.imageView1)).setImageBitmap(bmp);
	}
	
	private class getDatas extends AsyncTask<String, Void, String> {

		private ProgressDialog Dialog = new ProgressDialog(MainActivity.this);

		@Override
		protected void onPreExecute() {
			Dialog.setMessage("Sending Data ...");
			Dialog.show();
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "";
			String filepath = "";
			try
			{   
			  String root = Environment.getExternalStorageDirectory().toString();
			  File SDCardRoot = new File(root + "/saved_images");
			  SDCardRoot.mkdirs();
			  String filename="downloadedFile2.png";   
			  Log.i("Local filename:",""+filename);
			  File file = new File(SDCardRoot,filename);
			  
			  if (!file.exists()){
				  URL url = new URL("http://assets.kompas.com/data/2013/kompascom4/images/logokompascom.png");
				  HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
				  urlConnection.setRequestMethod("GET");
				  urlConnection.setDoOutput(true);                   
				  urlConnection.connect();
				  
				  
					if (file.createNewFile()) {
						file.createNewFile();
					}
					FileOutputStream fileOutput = new FileOutputStream(file);
					InputStream inputStream = urlConnection.getInputStream();
					int totalSize = urlConnection.getContentLength();
					int downloadedSize = 0;
					byte[] buffer = new byte[1024];
					int bufferLength = 0;
					while ((bufferLength = inputStream.read(buffer)) > 0) {
						fileOutput.write(buffer, 0, bufferLength);
						downloadedSize += bufferLength;
						Log.i("Progress:", "downloadedSize:" + downloadedSize
								+ "totalSize:" + totalSize);
					}
					fileOutput.close();

					if (downloadedSize == totalSize)
						filepath = file.getPath();
			  }else{
				  filepath = file.getPath();
			  }
			} 
			catch (MalformedURLException e) 
			{
			  e.printStackTrace();
			} 
			catch (IOException e)
			{
			  filepath=null;
			  e.printStackTrace();
			}
			Log.i("filepath:"," "+filepath) ;
			
			fp = filepath;
			return filepath;
		}

		@Override
		protected void onPostExecute(String result) {
			Dialog.dismiss();
			setGambar(fp);
		}
	}
}